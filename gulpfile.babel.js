import Collector from './includes/Collector';

var roots = {
	src:  'src',
	dest: 'build',
};

var collector = new Collector({

	parts: [
		{
			name: 'project',
			tasks: [
				{
					type: 'imgStatic',
					src: `${roots.src}/project/img/static/**/*.*`,
					dest: `${roots.dest}/project/img/static`,
					watch: `${roots.src}/project/img/static/*.*`,
				},
				{
					type: 'imgStatic',
					src: `${roots.src}/project/img/pic/**/*.*`,
					dest: `${roots.dest}/project/img/pic`,
					watch: `${roots.src}/project/img/pic/*.*`,
				},
				{
					type: 'imgSprite',
					src: `${roots.src}/project/img/sprite/**/*.*`,
					spriteDest: `${roots.dest}/project/img`,
					cssDest: `${roots.src}/project/temp/sass`,
					imgPath: '../img/sprite.png', // Путь спрайта для вставки в css
					cssName: 'sprite.sass',
					watch: `${roots.src}/project/img/sprite/**/*.*`,
				},
				{
					type: 'svgSprite',
					src: `${roots.src}/project/img/svg/**/*.*`,
					rawDest: `${roots.dest}/project/img/svg`,
					spriteDest: [
						`${roots.dest}/project/svg`,
						`${roots.src}/project/temp/svg`,
					],
					watch: `${roots.src}/project/img/svg/**/*.*`,
					templates: [`${roots.src}/project/config/svg-symbols.svg`],
				},
				{
					type: 'copy',
					src: `${roots.src}/project/fonts/**/*.*`,
					dest: `${roots.dest}/project/fonts`,
					watch: `${roots.src}/project/fonts/**/*.*`,
				},
				{
					type: 'copy',
					src: `${roots.src}/project/vendor/**/*.*`,
					dest: `${roots.dest}/project/vendor`,
					watch: `${roots.src}/project/vendor/**/*.*`,
				},
				{
					type: 'copy',
					src: `${roots.src}/project/audio/**/*.*`,
					dest: `${roots.dest}/project/audio`,
					watch: `${roots.src}/project/audio/**/*.*`,
				},
				{
					type: 'sass',
					src: `${roots.src}/project/sass/main.sass`,
					dest: `${roots.dest}/project/css/`,
					watch: [
						`${roots.src}/project/sass/main.sass`,
						`${roots.src}/project/components/**/*.sass`,
						`${roots.src}/lib/sass/**/*.*`,
						`${roots.src}/project/temp/sass/**/*.*`,
					],
				},
				{
					type: 'js',
					src: `${roots.src}/project/js/main.js`,
					dest: `${roots.dest}/project/js/`,
					watch: [
						`${roots.src}/project/js/main.js`,
						`${roots.src}/project/components/**/*.js`,
					],
				},
				{
					type: 'html',
					src: `${roots.src}/project/html/*.html`,
					dest: `${roots.dest}/project/`,
					watch: [
						`${roots.src}/project/html/*.html`,
						`${roots.src}/project/components/**/*.html`,
						`${roots.src}/project/temp/svg/svg-symbols.svg`,
					],
				},
			],
		},
	],

});

collector.run();